<h2>Build dependency graph for KDE Applications 18.08</h2>
	<p>We have only a graph of KDE PIM because it is a complex part inside KDE Applications. I added two packages from outside of the KDE PIM umbrella: libkgapi and libkolab. Both depend on KDE PIM stuff and KDE PIM stuff depends on them.</p>
	<div align="center">
		<a href="images/pim-build-deps-18.08.png"> <img alt="Build Dependency of source packages" src="images/pim-build-deps-18.08.png" width=700 /> </a></br>
		Click on the image to see it in its original size.
	</div>
	<div align="center">
                <h4>Tier view for KDE PIM</h4>
		<p>The simple dependency graph is not very useful if you want to feed a builder. Here the tiers are more useful. One tier is defined such that it only depends on things of lower tiers. That means you can build one tier in parallel. After building one tier you need to make sure that the next tier will use the newly-built packages.
		<a href="images/pim-build-tier-18.08.png"> <img alt="Build Dependency of source packages" src="images/pim-build-tier-18.08.png" width=700 /> </a></br>
		Click on the image to see it in its original size.
	</div>
	<br>
	<ul>
		<li>Packages with green background are inital packages; they have no dependencies inside KDE PIM.</li>
		<li>Packages with lightblue/diamonds background are end of chain. Nothing depends on them.</li>
	</ul>
	<p>
		The dot file for generating this graph can be downloaded: <a href="files/pim-build-tier-18.08.dot">pim-build-tier-18.08.dot</a>, <a href="files/pim-build-tier-18.08.dot">pim-build-tier-18.08.dot</a>.<br />
		The code for creating the dot file can be downloaded: <a href="files/pim-build-graph.py">pim-build-graph.py</a>, <a href="files/pim-build-tier.py">pim-build-tier.py</a>.
	</p>
	<p>
		Link to <a href="https://buildd.debian.org/status/package.php?p=kontactinterface%2Clibkf5grantleetheme%2Cakonadi%2Ckldap%2Ckpimtextedit%2Ckpkpass%2Ckcontacts%2Ckmime%2Ckdav%2Ckcalcore%2Csyndication%2Clibkf5libkleo%2Ckidentitymanagement%2Cksmtp%2Ckimap%2Ckitinerary%2Clibkgapi%2Cakonadi-notes%2Ckblog%2Ckmbox%2Cakonadi-mime%2Ckmailtransport%2Cakonadi-contacts%2Cakonadi-search%2Ckcalutils%2Ckleopatra%2Cktnef%2Ckgpg%2Clibkf5libkdepim%2Cakonadi-calendar%2Ckalarmcal%2Clibkf5pimcommon%2Clibkf5mailimporter%2Ckdepim-runtime%2Clibkf5ksieve%2Cknotes%2Ckmail-account-wizard%2Clibkf5gravatar%2Ckf5-kdepim-apps-libs%2Cpim-sieve-editor%2Ckontact%2Clibkf5calendarsupport%2Ckf5-messagelib%2Ckaddressbook%2Clibkf5eventviews%2Cakregator%2Clibkf5mailcommon%2Cakonadiconsole%2Cakonadi-calendar-tools%2Cgrantlee-editor%2Cakonadi-import-wizard%2Ckalarm%2Cmbox-importer%2Ckmail%2Clibkf5incidenceeditor%2Cpim-data-exporter%2Ckorganizer%2Ckdepim-addons&suite=sid">buildd build status</a>
	</p>

<h3>Upstream announcements<h3>
<ul>
	<li><a href="https://www.kde.org/announcements/announce-applications-18.08.0.php">KDE Applications 18.08.0</a></li>
	<li><a href="https://www.kde.org/announcements/announce-applications-18.08.1.php">KDE Applications 18.08.1</a></li>
	<li><a href="https://www.kde.org/announcements/announce-applications-18.08.2.php">KDE Applications 18.08.2</a></li>
	<li><a href="https://www.kde.org/announcements/announce-applications-18.08.3.php">KDE Applications 18.08.3</a></li>
</ul>
