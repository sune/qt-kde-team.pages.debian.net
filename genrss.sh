#!/bin/bash

FILE=pages/index

NEWS_START=`grep -n "RSS_START" ${FILE}  | sed 's/:.*//'`

let "NEWS_START += 1"

NEWS_END=`tail -n +${NEWS_START} ${FILE} | grep -n "RSS_END" | sed 's/:.*//'`

let "NEWS_END -= 1"

cat <<__EOHEAD__ 
<?xml version="1.0" encoding="iso-8859-1"?>
<rss version="2.0">
<channel>
<title>Debian KDE maintainers</title>
<description>Debian KDE news feed</description>
<link>http://qt-kde-team.pages.debian.net/www/</link>
<lastBuildDate>`date -R`</lastBuildDate>
<generator>bash hacking</generator>
<image>
  <url>http://qt-kde-team.pages.debian.net/www/images/kdedebianlogo2_64.png</url>
  <title>Debian KDE maintainers</title>
  <link>http://qt-kde-team.pages.debian.net/www/</link>
  <description>Debian KDE news feed</description>
</image>
__EOHEAD__

tail -n +${NEWS_START} ${FILE} | head -n +${NEWS_END} | sed -r 's,<p>,<item>,g;s,</p>,]]></description></item>,g;s,<strong>,<title>,g;s,</strong>,</title>,g;s,(</title>\s*)<br />,\1<link>http://qt-kde-team.pages.debian.net/www/</link><description><![CDATA[,g'

cat << __EOFOOT__
        </channel>
</rss>

__EOFOOT__

